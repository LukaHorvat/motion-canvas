import { makeScene2D } from '@motion-canvas/2d/lib/scenes';
import { Circle, Layout, Shape, ShapeProps, Text, View2D, Rect, Line } from '@motion-canvas/2d/lib/components';
import { createRef, range, Reference } from '@motion-canvas/core/lib/utils';
import { createSignal, SignalValue, SimpleSignal } from '@motion-canvas/core/lib/signals';
import { initial, signal } from '@motion-canvas/2d/lib/decorators';
import { Vector2 } from '@motion-canvas/core/lib/types';
import { arc } from "@motion-canvas/2d/lib/utils";
import { easeInExpo, easeInOutBounce, easeInOutCubic, easeInOutQuad, easeInQuad, easeOutQuad } from '@motion-canvas/core/lib/tweening';
import { all, loop, sequence } from '@motion-canvas/core/lib/flow';

export interface GearProps extends ShapeProps {
    radius: SignalValue<number>;
    teeth: SignalValue<number>;
}

export class Gear extends Shape {
    @initial(100)
    @signal()
    public declare readonly radius: SimpleSignal<number, this>;
    @initial(10)
    @signal()
    public declare readonly teeth: SimpleSignal<number, this>;

    public constructor(props: GearProps) {
        super(props);
    }

    protected override getPath(): Path2D {
        const path = new Path2D();
        let teeth = Math.round(this.teeth());
        for (let i = 0; i < teeth; i++) {
            let ang = (ofs: number) => ((i + ofs) / teeth) * Math.PI * 2;
            arc(path, Vector2.zero, this.radius() * 0.8, ang(-0.4), ang(0), false);
            arc(path, Vector2.zero, this.radius(), ang(0.1), ang(0.5), false);
        }
        return path;
    }
}

function* blink(circle: Reference<Circle>) {
    yield* circle().scale(1.2, 0.15, easeInQuad);
    yield* circle().scale(1, 0.15, easeOutQuad);
}

export interface ConnectLineProps extends ShapeProps {
    decay: SignalValue<number>;
    start: SignalValue<Vector2>;
    end: SignalValue<Vector2>;
    numSegments: SignalValue<number>;
}

export class ConnectLine extends Shape {
    @initial(0)
    @signal()
    public declare readonly decay: SimpleSignal<number, this>;
    @initial(Vector2.zero)
    @signal()
    public declare readonly start: SimpleSignal<Vector2, this>;
    @initial(Vector2.zero)
    @signal()
    public declare readonly end: SimpleSignal<Vector2, this>;
    @initial(4)
    @signal()
    public declare readonly numSegments: SimpleSignal<number, this>;

    public constructor(props: ConnectLineProps) {
        super(props);
    }

    protected override getPath(): Path2D {
        const path = new Path2D();
        let decay = this.decay();
        let start = this.start();
        let end = this.end();
        let numSegments = this.numSegments();

        path.moveTo(start.x, start.y);
        for (let i = 1; i <= numSegments; i++) {
            let t = i / numSegments;
            let previous = Vector2.lerp(start, end, (i - 1) / numSegments);
            let current = Vector2.lerp(start, end, i / numSegments);
            let p = Vector2.lerp(previous, current, Math.min((decay + t * t) * decay, 1));
            let ofs = current.sub(p).scale(0.5);
            previous = previous.add(ofs);
            p = p.add(ofs);
            path.moveTo(previous.x, previous.y);
            path.lineTo(p.x, p.y);
        }

        return path;
    }
}

export interface CurvedTextProps extends ShapeProps {
    text: SignalValue<string>;
    radius: SignalValue<number>;
    // Angle offset in degrees
    angleOffset: SignalValue<number>;
    // End angle in degrees
    endAngle: SignalValue<number>;
}

export class CurvedText extends Shape {
    @initial("")
    @signal()
    public declare readonly text: SimpleSignal<string, this>;
    @initial(100)
    @signal()
    public declare readonly radius: SimpleSignal<number, this>;
    @initial(0)
    @signal()
    public declare readonly angleOffset: SimpleSignal<number, this>;
    @initial(0)
    @signal()
    public declare readonly endAngle: SimpleSignal<number, this>;

    public constructor(props: CurvedTextProps) {
        super(props);
    }

    protected override draw(ctx: CanvasRenderingContext2D) {
        ctx.font = "30px Segoe UI";
        ctx.fillStyle = "white";
        ctx.textAlign = "center";
        ctx.textBaseline = "middle";
        let text = this.text();
        let radius = this.radius();
        let angleOffset = this.angleOffset();
        let endAngle = this.endAngle();
        for (let i = 0; i < text.length; i++) {
            let angle = ((i / text.length) * endAngle + angleOffset) * Math.PI / 180;
            let x = Math.cos(angle) * radius;
            let y = Math.sin(angle) * radius;
            ctx.save();
            ctx.translate(x, y);
            ctx.rotate(angle + Math.PI / 2);
            ctx.fillText(text[i], 0, 0);
            ctx.restore();
        }
    }
}



function* connect(view: View2D, point1: Vector2, point2: Vector2) {
    let line = createRef<ConnectLine>();
    let endPoint = createSignal(point1);
    view.add(<ConnectLine
        ref={line}
        start={point1}
        end={() => endPoint()}
        lineWidth={0}
        stroke={'white'}
        decay={1}
        numSegments={5}
    />);
    yield* all(endPoint(point2, 0.25, easeInExpo), line().lineWidth(15, 0.25, easeInExpo));
    yield* all(line().decay(0, 1), line().lineWidth(0, 1));
    line().remove();
}

function* encircle(view: View2D, point1: Vector2, radius: number, angleOffset: number = 0) {
    let circle = createRef<Circle>();
    view.add(<Circle
        ref={circle}
        width={radius * 2}
        height={radius * 2}
        stroke="white"
        lineWidth={15}
        x={point1.x}
        y={point1.y}
        startAngle={angleOffset}
        endAngle={angleOffset}>
    </Circle>);
    yield* circle().endAngle(angleOffset + 360, 0.5);
    yield* circle().startAngle(angleOffset + 360, 0.5);
    circle().remove();
}

function* path(view: View2D, from: Vector2, to: Vector2) {
    let xMoves = range(10).map(i => Math.random()).sort();
    let yMoves = range(10).map(i => Math.random()).sort();
    let points = [];
    points.push(from);
    let lineLength = 0;
    for (let i = 0; i < 10; i++) {
        let lastX = i == 0 ? from.x : points[points.length - 1].x;
        let lastY = i == 0 ? from.y : points[points.length - 1].y;
        let x = from.x + (to.x - from.x) * xMoves[i];
        let y = from.y + (to.y - from.y) * yMoves[i];
        lineLength += Math.sqrt((x - lastX) * (x - lastX) + (y - lastY) * (y - lastY));
        points.push(new Vector2(x, y));
    }
    lineLength += Math.sqrt((to.x - points[points.length - 1].x) * (to.x - points[points.length - 1].x) + (to.y - points[points.length - 1].y) * (to.y - points[points.length - 1].y));
    points.push(to);
    let line = createRef<Line>();
    view.add(<Line
        ref={line}
        points={points}
        lineWidth={15}
        stroke="white"
        endOffset={lineLength}
    />);
    yield* line().endOffset(0, 0.5);
    yield* line().startOffset(lineLength, 0.5);
    line().remove();

}

let lastPoint = new Vector2(0, 0);

export default makeScene2D(function* (view) {
    yield* loop(10, () => {
        let newPoint = new Vector2((Math.random() - 0.5) * view.width(), (Math.random() - 0.5) * view.height());
        let gen = path(view, lastPoint, newPoint);
        lastPoint = newPoint;
        return gen;
    });
});